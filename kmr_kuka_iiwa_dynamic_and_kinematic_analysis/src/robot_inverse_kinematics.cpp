#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <Eigen/Dense>
using namespace std;

double gen_rand(double min, double max)
{
    int rand_num = rand() % 100 + 1;
    double result = min + (double)((max - min) * rand_num) / 101.0;
    return result;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "end_effector_transformation_matrix");
    KDL::Tree my_tree;
    ros::NodeHandle node;
    string robot_desc_string;
    node.param("/iiwa/robot_description", robot_desc_string, string());
    if (!kdl_parser::treeFromString(robot_desc_string, my_tree))
    {
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain kuka_arm_chain;
    my_tree.getChain("iiwa_link_0", "iiwa_link_7", kuka_arm_chain);

    int number_of_joints = kuka_arm_chain.getNrOfJoints();
    ROS_WARN("Number of joints: %d\n", number_of_joints);

    double eps=1E-5;
    int maxiter=500;
    double eps_joints=1E-15;
    KDL::ChainIkSolverPos_LMA iksolver = KDL::ChainIkSolverPos_LMA(kuka_arm_chain,eps,maxiter,eps_joints);

    KDL::JntArray jointpositions = KDL::JntArray(number_of_joints);
    KDL::JntArray jointGuesspositions = KDL::JntArray(number_of_joints);

    KDL::Vector pos_target = KDL::Vector(-0.46164,0.33849,-0.04943);
    KDL::Rotation rot_target;
    rot_target.EulerZYX(-0.5,0,0); 
    KDL::Frame T_goal(rot_target,pos_target);
    bool kinematics_status;
    kinematics_status = iksolver.CartToJnt(jointGuesspositions,T_goal,jointpositions);
    if(kinematics_status>=0){
        for(int i=0;i<number_of_joints;i++){
            std::cout << jointpositions(i) << std::endl;
        }
        printf("%s \n","Success, thanks KDL!");
    }
    else{
        printf("%s \n","Error:could not calculate backword kinematics : ");
    }



}


