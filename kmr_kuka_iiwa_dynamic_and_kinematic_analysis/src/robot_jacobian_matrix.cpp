#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <Eigen/Dense>
using namespace std;

double gen_rand(double min, double max)
{
    int rand_num = rand() % 100 + 1;
    double result = min + (double)((max - min) * rand_num) / 101.0;
    return result;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "end_effector_transformation_matrix");
    KDL::Tree my_tree;
    ros::NodeHandle node;
    string robot_desc_string;
    node.param("/iiwa/robot_description", robot_desc_string, string());
    if (!kdl_parser::treeFromString(robot_desc_string, my_tree))
    {
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain kuka_arm_chain;
    my_tree.getChain("iiwa_link_0", "iiwa_link_7", kuka_arm_chain);

    int number_of_joints = kuka_arm_chain.getNrOfJoints();
    ROS_WARN("Number of joints: %d\n", number_of_joints);

    KDL::ChainJntToJacSolver jacobsolver(kuka_arm_chain);
    KDL::Jacobian jacobian_matrix(number_of_joints);
    KDL::JntArray q_in(number_of_joints);
    for (int j = 0; j < number_of_joints; j++)
    {
        // generate random numbers between -pi and pi for the joints
        q_in(j) = gen_rand(-M_PI, M_PI);
    }

    jacobsolver.JntToJac(q_in, jacobian_matrix);

    std::cout << "Jacobian Matrix" << std::endl;
    std::cout << jacobian_matrix.columns() << std::endl;
    std::cout << jacobian_matrix.rows() << std::endl;
    Eigen::MatrixXd jacb_matr;
    jacb_matr = jacobian_matrix.data;
    std::cout << jacb_matr << std::endl;
}