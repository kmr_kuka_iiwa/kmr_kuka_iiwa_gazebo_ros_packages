#!/usr/bin/python
import rospy
from std_msgs.msg import String
from gazebo_msgs.msg import ModelStates
import tf
import numpy as np
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
from std_msgs.msg import Header
from math import pi as pi_number
origin, xaxis, yaxis, zaxis = (0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1)
from tf.transformations import euler_from_quaternion, quaternion_from_euler

def PoseStamped_2_mat(p):
    #Get Transformation matrix from pose and quartenions
    q = p.pose.orientation
    pos = p.pose.position
    T = tf.transformations.quaternion_matrix([q.x,q.y,q.z,q.w])
    #adding positions to the positon part of the transformation matrix
    T[:3,3] = np.array([pos.x,pos.y,pos.z])
    return T

if __name__ == "__main__":
    print('test')
    rospy.init_node('kmr_robot_listener')
    # rospy.sleep(5)
    print("After sleep")
    listener = tf.TransformListener()
    listener.waitForTransform("/iiwa_link_1", "/iiwa_link_2", rospy.Time(), rospy.Duration(4.0))
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        try:
            (robot_location,rot) = listener.lookupTransform('/iiwa_link_1','/iiwa_link_2', rospy.Time(0))
            print("End effector location with respect to world frame",robot_location)
            print(robot_location)
            print(rot)
            euler_angles = euler_from_quaternion(rot)
            print(euler_angles)
            rate.sleep()
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
    
    rospy.spin()
