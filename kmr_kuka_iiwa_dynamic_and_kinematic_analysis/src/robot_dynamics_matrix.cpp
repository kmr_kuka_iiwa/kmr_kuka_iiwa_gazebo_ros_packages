#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <Eigen/Dense>
using namespace std;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "end_effector_transformation_matrix");
    KDL::Tree my_tree;
    ros::NodeHandle node;
    string robot_desc_string;
    node.param("/iiwa/robot_description", robot_desc_string, string());
    if (!kdl_parser::treeFromString(robot_desc_string, my_tree))
    {
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain kuka_arm_chain;
    my_tree.getChain("iiwa_link_0", "iiwa_link_7", kuka_arm_chain);

    int number_of_joints = kuka_arm_chain.getNrOfJoints();
    ROS_WARN("Number of joints: %d\n", number_of_joints);

    KDL::JntArray q(7);
    // Set zero for all the joint variables because the robot in the above picture has zero joint angles for all joints
    q(0) = 0.3;
    q(1) = 0.3;
    q(2) = 0.3;
    q(3) = 0.3;
    q(4) = 0.3;
    q(5) = 0.3;
    q(6) = 0.3;

    KDL::Vector g(0, 0, -9.81);
    KDL::ChainDynParam dyn_params(kuka_arm_chain, g);

    KDL::JntSpaceInertiaMatrix M(7);
    dyn_params.JntToMass(q, M);
    Eigen::MatrixXd Mass_Matrix(7, 7);
    ROS_WARN("%d %d\n", M.columns(), M.rows());
    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            // std::printf("%f, ", M(i, j));
            // std::printf("\n");
            Mass_Matrix(i, j) = M(i, j);
        }
    }

    std::cout << "Mass Matrix: " << std::endl;
    std::cout << Mass_Matrix << std::endl;

    KDL::JntArray G(number_of_joints);
    Eigen::VectorXd Coriolis_Matrix(number_of_joints);
    KDL::JntArray q_dot(number_of_joints);
    q_dot(1) = 0.3;
    dyn_params.JntToCoriolis(q,q_dot, G);

     ROS_WARN("%d %d\n", G.columns(), G.rows());
    for(int i=0;i<number_of_joints;i++) {
            // std::printf("%f, ", G(i));
            Coriolis_Matrix(i) = G(i);

    }

    std::cout << "Coriolis Matrix: " << std::endl;
    std::cout << Coriolis_Matrix << std::endl;

    
    KDL::JntArray gra(number_of_joints);
    Eigen::VectorXd Gravity_Matrix(number_of_joints);
    dyn_params.JntToGravity(q,gra);

     ROS_WARN("%d %d\n", gra.columns(), gra.rows());
    for(int i=0;i<number_of_joints;i++) {
            Gravity_Matrix(i) = gra(i);
    }

    std::cout << "Gravity_Matrix: " << std::endl;
    std::cout << Gravity_Matrix << std::endl;




}
