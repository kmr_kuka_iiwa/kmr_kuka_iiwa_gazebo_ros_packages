#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>

using namespace std;

int main(int argc, char **argv) {
    ros::init(argc, argv, "get_number_of_joint_kdl_script");
    KDL::Tree my_tree;
    ros::NodeHandle node;
    string robot_desc_string;
    node.param("/iiwa/robot_description", robot_desc_string, string());
    if (!kdl_parser::treeFromString(robot_desc_string, my_tree)){
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain r_arm_chain;
    my_tree.getChain("iiwa_link_0", "iiwa_link_7", r_arm_chain);

    ROS_WARN("Number of joints: %d\n", r_arm_chain.getNrOfJoints());
}
