#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <Eigen/Dense>
using namespace std;

double gen_rand(double min, double max)
{
    int rand_num = rand() % 100 + 1;
    double result = min + (double)((max - min) * rand_num) / 101.0;
    return result;
}


int main(int argc, char **argv) {
    ros::init(argc, argv, "end_effector_transformation_matrix");
    KDL::Tree my_tree;
    ros::NodeHandle node;
    string robot_desc_string;
    node.param("/iiwa/robot_description", robot_desc_string, string());
    if (!kdl_parser::treeFromString(robot_desc_string, my_tree)){
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain kuka_arm_chain;
    my_tree.getChain("iiwa_link_0", "iiwa_link_7", kuka_arm_chain);

    int number_of_joints =  kuka_arm_chain.getNrOfJoints();
    ROS_WARN("Number of joints: %d\n", number_of_joints);

    KDL::JntArray q(7);
    // Set zero for all the joint variables because the robot in the above picture has zero joint angles for all joints
    // q(0) = 0;
    // q(1) = 0;
    // q(2) = 0;
    // q(3) = 0;
    // q(4) = 0;
    // q(5) = 0;
    // q(6) = 0;
    // Generate random number between -Pi and Pi
    
    q(0) = gen_rand(0, M_PI);
    q(1) = gen_rand(0, M_PI);
    q(2) = gen_rand(0, M_PI);
    q(3) = gen_rand(0, M_PI);
    q(4) = gen_rand(0, M_PI);
    q(5) = gen_rand(0, M_PI);
    q(6) = gen_rand(0, M_PI);
    // q_in(0) = gen_rand(-M_PI, M_PI);
    KDL::ChainFkSolverPos_recursive fksolver(kuka_arm_chain);
    KDL::Frame T;
    fksolver.JntToCart(q,T);


    std::cout << "q(theta1) = " << q(0) << std::endl;
    std::cout << "q(theta2) = " << q(1) << std::endl;
    std::cout << "q(theta3) = " << q(2) << std::endl;
    std::cout << "q(theta4) = " << q(3) << std::endl;
    std::cout << "q(theta5) = " << q(4) << std::endl;
    std::cout << "q(theta6) = " << q(5) << std::endl;
    std::cout << "q(theta7) = " << q(6) << std::endl;

    // We can convert T matrix to the EigenMatrix to be able to print out in nice way
    std::cout << "Transformation Matrix: " << std::endl;
        
    Eigen::MatrixXd Frame_Matrix(4,4);
    Frame_Matrix << T(0,0),T(0,1),T(0,2),T(0,3),
                    T(1,0),T(1,1),T(1,2),T(1,3),
                    T(2,0),T(2,1),T(2,2),T(2,3),
                    T(3,0),T(3,1),T(3,2),T(3,3);

    std::cout << Frame_Matrix << std::endl;
    



}
