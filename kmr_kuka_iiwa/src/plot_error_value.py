import matplotlib.pyplot as plt
import numpy as np 
import scipy.io
# error_value = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/error_x.npy')

trajectory_x = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_x.npy')
trajectory_y = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/mobile_robot_trajectory_y.npy')

# desired_trajectory_x = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/desired_trajetory_x.npy')
# desired_trajectory_y = np.load('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/desired_trajetory_y.npy')


desired_trajectory_x =  scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajectory_x.mat')
desired_trajectory_x = np.array(desired_trajectory_x['trajectory_x'])

desired_trajectory_y =  scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajectory_y.mat')
desired_trajectory_y = np.array(desired_trajectory_y['trajectory_y'])


plt.plot(trajectory_x,trajectory_y,'r.' )
plt.plot(desired_trajectory_x,desired_trajectory_y,'.' )


# plt.plot(desired_trajectory_x,desired_trajectory_y )
plt.ylabel('some numbers')
plt.show()