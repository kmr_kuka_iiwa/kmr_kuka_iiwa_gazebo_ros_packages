#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist
from gazebo_msgs.msg import ModelStates
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np 
import scipy.io
import tf2_ros


# iteration_counter = 0
# trajectory_x = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajectory_x.mat')
# trajectory_x = np.array(trajectory_x['trajectory_x'])

# trajectory_y = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajectory_y.mat')
# trajectory_y = np.array(trajectory_y['trajectory_y'])

# trajectory_phi = scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajectory_phi.mat')
# trajectory_phi = np.array(trajectory_phi['trajectory_phi'])


velocity_container =  scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/velocity.mat')
velocity_container = np.array(velocity_container['velocity_container'])

angular_container =  scipy.io.loadmat('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/angular.mat')
angular_container = np.array(angular_container['angular_container'])



tmp_time = 0

real_trajectory_x = []
real_trajectory_y = []

desired_trajectory_x = []
desired_trajectory_y = []

desired_pos_x = 0
desired_pos_y = 0
theta_desired = 0 
def shutdown_function():
    print("Controller shut down")
    twist = Twist()
    twist.linear.x = 0.0; twist.linear.y = 0.0; twist.linear.z = 0.0
    twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 0.0
    velocity_publisher.publish(twist)

def model_position(data):

    object_list_in_gazebo = data.name
    all_objects_startswith = [i for i in data.name if i.startswith("kmr")] # All objects in Gazebo starts with specified string 
    kmr_index_number_in_object_list = object_list_in_gazebo.index('kmr')
    
    mobile_robot_position = data.pose[kmr_index_number_in_object_list].position

    print(mobile_robot_position)

    real_trajectory_x.append(mobile_robot_position.x)
    real_trajectory_y.append(mobile_robot_position.y)

    np.save('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajetory_x.npy',np.array(real_trajectory_x))
    np.save('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/trajetory_y.npy',np.array(real_trajectory_y))

    mobile_robot_orientation = data.pose[kmr_index_number_in_object_list].orientation

    (robot_roll, robot_pitch, robot_yaw) = euler_from_quaternion([mobile_robot_orientation.x,mobile_robot_orientation.y,mobile_robot_orientation.z,mobile_robot_orientation.w])

    global iteration_counter
    global theta_velocity_history
    global tmp_time
    global desired_pos_x
    global desired_pos_y
    global theta_desired
    # print("Robot yaw " ,robot_yaw)
    # print(theta_velocity_history.shape)


    current_time = rospy.get_time()

    twist_target = Twist()
    twist_target.linear.x = 0
    twist_target.linear.y = 0
    twist_target.linear.z = 0
    twist_target.angular.x = 0
    twist_target.angular.y = 0
    twist_target.angular.z = 0
    
    desired_pos_x = trajectory_x[0,iteration_counter]
    desired_pos_y = trajectory_y[0,iteration_counter]
    theta_desired = trajectory_phi[0,iteration_counter]
    err_loc = -1 * np.array([mobile_robot_position.x -desired_pos_x,mobile_robot_position.y - desired_pos_y ,robot_yaw-theta_desired])

    k1 = 1
    k2 = 5
    v_in = k1 * np.sqrt(err_loc[0]**2 + err_loc[1]**2)
    w_in = k2 * (np.arctan2(-err_loc[1],err_loc[0]) - robot_yaw)

    twist = Twist()
    twist.linear.x = 0
    twist.linear.y = 0.0
    twist.linear.z = 0.0
    twist.angular.x = 0.0
    twist.angular.y = 0.0
    twist.angular.z = 0


    # theta_desired = theta_desired  + twist.angular.z * 0.001
    # desired_pos_x = desired_pos_x + twist.linear.x * math.cos(theta_desired) * 0.001
    # desired_pos_y = desired_pos_y + twist.linear.x * math.sin(theta_desired) * 0.001







    # np.save('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/desired_trajetory_x.npy',np.array(desired_trajectory_x))
    # np.save('/home/erdi/Documents/NRP/GazeboRosPackages/src/kmr_kuka_iiwa_gazebo_ros_packages/kmr_kuka_iiwa/src/desired_trajetory_y.npy',np.array(desired_trajectory_y))



    iteration_counter = iteration_counter + 1 

    velocity_publisher.publish(twist)
    try:
        print("frequency:",current_time-tmp_time)
    except:
        pass

    tmp_time = rospy.get_time()

    print(iteration_counter)


while_loop_counter = 0
if __name__ == "__main__":

    # ROS Configurations
    rospy.init_node("kmr_kuka_robot_mobile_part_controller")
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    rate = rospy.Rate(10) # 10hz
    velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    # rospy.Subscriber("/gazebo/model_states", ModelStates, model_position)
    rospy.on_shutdown(shutdown_function)

    while not rospy.is_shutdown():
        while_loop_counter += 1
        # velocity_publisher.publish(twist)
        try:
            trans = tfBuffer.lookup_transform('odom', 'iiwa_link_7', rospy.Time())
            print(trans.transform.translation)

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("In Except situation")
        
        
        
        twist = Twist()
        twist.linear.x = velocity_container[0,while_loop_counter]
        twist.linear.y = 0.0
        twist.linear.z = 0.0
        twist.angular.x = 0.0
        twist.angular.y = 0.0
        twist.angular.z = angular_container[0,while_loop_counter]
        
        velocity_publisher.publish(twist)

        print(rospy.get_time())
        print(while_loop_counter)
        rate.sleep()

    
    
    rospy.spin()